#An API call to get GPS coordinates of all active Translink buses at that moment.
__author__='Jacky Liao'
import sys, urllib2, json,datetime
path=sys.argv[1]
key='Rhc2BeSOxkSqRkLM949K'
url='http://api.translink.ca/rttiapi/v1/buses?apikey='+key

req=urllib2.Request(url)
req.add_header('content-type','application/JSON')
jlist=json.load(urllib2.urlopen(req))

output=path+str(datetime.datetime.now())+'.json'
my_file=open(output,'w')
json.dump(jlist,my_file)
my_file.close()

#python get_data.py ../RAW