#plottTAC.py will read from the Temperary table from tripTimeAndCount.py, and perform
#calculation to find average trip time and average bus count and plot the result.
__author__='Jacky Liao'
from pyspark import SparkConf,SparkContext,sql
from pyspark.sql.types import StructType,StructField,StringType,DateType,IntegerType,FloatType,TimestampType
from pyspark.sql import functions as F
from pyspark.sql.window import Window
from getDistance import distance_on_unit_sphere as get_dist
import sys,re,datetime,json
import matplotlib.pyplot as plt
import matplotlib

#T1=starting time & T2=ending time; return the time past
def get_time(T1,T2):
    deltaT=T2-T1
    #deltaT in minutes
    deltaT=deltaT.seconds/60.0
    return deltaT

def weekday(TS):
	return TS.weekday()   

def main(sc,input):
    sqlContext=sql.SQLContext(sc)
    df=sqlContext.read.parquet(input).cache()

    date_list=df.select(F.to_date(df.T2).alias('Date')).dropDuplicates().collect()
    date1=str(date_list[0].Date)
    date2=str(date_list[len(date_list)-1].Date)
#let's just assume we only care about the status at SFU/Burrad STN
    direction=df.select(df.Direction).dropDuplicates().collect()
    if direction[0].Direction=='EAST':
        destination='SFU'
        lastStop=53096
    elif direction[0].Direction=='WEST':
        destination='Burrard STN'
        lastStop=50530

    DoW='Weekday'
#Average across Weekdays
#Bus Count
    WeeklyDf=df.where((df.EndNo==lastStop)&(df.DayofWeek==DoW)).cache()
    n=WeeklyDf.groupBy(F.to_date(WeeklyDf.T2)).agg(F.first(WeeklyDf.DayofWeek)).count()
    df1=WeeklyDf.groupBy(WeeklyDf.TimeBlock).agg((F.first(WeeklyDf.BusCount)).alias('AvgExpectedBusCount'),((F.count(WeeklyDf.RouteNo)/n)).alias('AvgActualBusCount')).orderBy(WeeklyDf.TimeBlock)
    rdd1=df1.rdd.cache()
#Lists associated to Bus Count
    WeeklyTimeBlockListBC=rdd1.map(lambda row:row.TimeBlock).collect()
    WeeklyAvgEBC=rdd1.map(lambda row:row.AvgExpectedBusCount).collect()
    WeeklyAvgBC=rdd1.map(lambda row:row.AvgActualBusCount).collect()

#TripTime
    deltaTudf=F.udf(lambda time1,time2:get_time(time1,time2),FloatType())
    df2=WeeklyDf.withColumn('ActualT',deltaTudf(WeeklyDf.T1,WeeklyDf.T2))
    df2=df2.groupBy(df2.TimeBlock).agg((F.first(df2.AvgT)).alias('AvgExpectedTravelTime'),((F.sum(df2.ActualT)/F.count(df2.ActualT))).alias('AvgTravelTime')).orderBy(df2.TimeBlock)
    rdd2=df2.rdd.cache()
#Lists associated to TripTime
    WeeklyTimeBlockListT=rdd2.map(lambda row:row.TimeBlock).collect()
    WeeklyAvgET=rdd2.map(lambda row:row.AvgExpectedTravelTime).collect()
    WeeklyAvgT=rdd2.map(lambda row:row.AvgTravelTime).collect()

    font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 25}
    matplotlib.rc('font', **font)

    fig1=plt.figure()
    diffBC=[WeeklyAvgBC[i]-WeeklyAvgEBC[i] for i in range(0,len(WeeklyAvgEBC))]
    ax1=fig1.add_subplot(1,3,1)
    ax1.bar(WeeklyTimeBlockListBC,WeeklyAvgBC,1)
    ax1.set_ylim([0,14])
    ax1.set_xlim([0,24])
    plt.xlabel('Hour of Day')
    plt.ylabel('Average Number of Buses')
    plt.title('Actual')
    plt.grid(True)
    ax2=fig1.add_subplot(1,3,2)
    ax2.bar(WeeklyTimeBlockListBC,WeeklyAvgEBC,1)
    ax2.set_ylim([0,14])
    ax2.set_xlim([0,24])
    plt.xlabel('Hour of Day')
    plt.ylabel('Number of Buses')
    plt.title('Scheduled')
    plt.grid(True)
    ax3=fig1.add_subplot(1,3,3)
    ax3.bar(WeeklyTimeBlockListBC,diffBC,1,color='r')
    plt.xlabel('Hour of Day')
    plt.title('Actual vs. Scheduled')
    plt.grid(True)
    fig1.text(0.2,0.96,'Weekday\'s Average Bus Count for 135 at '+destination+' from '+date1+' to '+date2)

    fig2=plt.figure()
    diffT=[WeeklyAvgT[i]-WeeklyAvgET[i] for i in range(0,len(WeeklyAvgET))]
    ax4=fig2.add_subplot(1,3,1)
    ax4.bar(WeeklyTimeBlockListT,WeeklyAvgT,1)
    ax4.set_xlim([0,24])
    plt.xlabel('Hour of Day')
    plt.ylabel('Average Trip Time (Minutes)')
    plt.title('Actual')
    plt.grid(True)
    ax5=fig2.add_subplot(1,3,2)
    ax5.bar(WeeklyTimeBlockListT,WeeklyAvgET,1)
    ax5.set_xlim([0,24])
    plt.xlabel('Hour of Day')
    plt.ylabel('Trip Time (Minutes)')
    plt.title('Scheduled')
    plt.grid(True)
    ax6=fig2.add_subplot(1,3,3)
    ax6.bar(WeeklyTimeBlockListT,diffT,1,color='r')
    ax6.set_xlim([0,24])
    plt.xlabel('Hour of Day')
    plt.ylabel('Average Delays (Minutes)')
    plt.title('Actual vs. Scheduled')
    plt.grid(True)
    fig2.text(0.2,0.96,'Weekday\'s Average Bus Trip Time for 135 at '+destination+' from '+date1+' to '+date2)
    plt.show()

#This code will give a more indepth analysis to the data, average across the same week day.
'''
#a list from 0 to 6; each integer represent the day of week minus one.
    DoWi_List=range(0,7)
    TimeBlockListBC=[]
    AvgEBC=[]
    AvgBC=[]
    TimeBlockListT=[]
    AvgET=[]
    AvgT=[]
for DoWi in DoWi_List:
	weekdayUDF=F.udf(lambda TS:weekday(TS),IntegerType())
#df.where(df.DayofWeek=='Sat').orderBy(df.T2).groupBy(df.TimeBlock).agg(F.count(df.TripId)).show(200)
#Bus Count
	DoWdf=df.where((df.EndNo==lastStop)&(weekdayUDF(df.T2)==DoWi)).cache()
	n=DoWdf.groupBy(F.to_date(DoWdf.T2)).agg(F.first(DoWdf.DayofWeek)).count()
	df1=DoWdf.groupBy(DoWdf.TimeBlock).agg((F.first(DoWdf.BusCount)).alias('AvgExpectedBusCount'),((F.count(DoWdf.RouteNo)/n)).alias('AvgActualBusCount')).orderBy(DoWdf.TimeBlock)
	rdd1=df1.rdd.cache()
#Lists associated to Bus Count
	TimeBlockListBC.append(rdd1.map(lambda row:row.TimeBlock).collect())
	AvgEBC.append(rdd1.map(lambda row:row.AvgExpectedBusCount).collect())
	AvgBC.append(rdd1.map(lambda row:row.AvgActualBusCount).collect())
#TripTime
	deltaTudf=F.udf(lambda time1,time2:get_time(time1,time2),FloatType())
	df2=DoWdf.withColumn('ActualT',deltaTudf(DoWdf.T1,DoWdf.T2))
	df2=df2.groupBy(df2.TimeBlock).agg((F.first(df2.AvgT)).alias('AvgExpectedTravelTime'),((F.sum(df2.ActualT)/F.count(df2.ActualT))).alias('AvgTravelTime')).orderBy(df2.TimeBlock)
	rdd2=df2.rdd.cache()
#Lists associated to TripTime
	TimeBlockListT.append(rdd2.map(lambda row:row.TimeBlock).collect())
	AvgET.append(rdd2.map(lambda row:row.AvgExpectedTravelTime).collect())
	AvgT.append(rdd2.map(lambda row:row.AvgTravelTime).collect())

print '********************************************************\n'
print TimeBlockListBC
print '********************************************************\n'
print AvgEBC
print '********************************************************\n'
print AvgBC
print '********************************************************\n'
print TimeBlockListT
print '********************************************************\n'
print AvgET
print '********************************************************\n'
print AvgT
print '********************************************************\n'
'''


if __name__=="__main__":
    input=sys.argv[1]
    conf=SparkConf().setAppName('Plot tripTimeAndCount')
    sc=SparkContext(conf=conf)
    main(sc,input)

#${SPARK_HOME}/bin/spark-submit --master 'local[*]' plottTAC.py ../TempTable
#    input='../TempTable' path to the temporary store of tripTimeAndCount.py output