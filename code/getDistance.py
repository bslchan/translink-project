#general structure taken from the web
import math

def distance_on_unit_sphere(lattitude1, longitutde1, lattitude2, longitutde2):
    #earth's radius in km
    R = 6373.0

    lat1 = math.radians(lattitude1)
    lon1 = math.radians(longitutde1)
    lat2 = math.radians(lattitude2)
    lon2 = math.radians(longitutde2)

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a = math.sin(dlat / 2)**2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon / 2)**2
    c = 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

    distance = R * c

    return distance