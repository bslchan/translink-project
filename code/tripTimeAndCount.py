#tripTimeAndCount.py try to find the trip time and bus count
#its task is to retreive relevant information related to the query from the MasterTable
#and join it with the Patterns and ScheduleTable and output a temperary parquet format for plottTAC.py to perform
#calculations and plotting.
__author__='Jacky Liao'
from pyspark import SparkConf,SparkContext,sql
from pyspark.sql.types import StructType,StructField,StringType,DateType,IntegerType,FloatType,TimestampType
from pyspark.sql import functions as F
from pyspark.sql.window import Window
import sys,re,datetime,json

#retrieve the code word of the day of the week
def get_DoW(DT):
    weekday=DT.weekday()
    if weekday==6:
	return 'Sun'
    elif weekday==5:
	return 'Sat'
    else:
	return 'Weekday'

def main(sc,input1,input2,input3,output,date1,date2,direction):
#load raw data to Hive data frame
    hiveContext=sql.HiveContext(sc)
    Master=hiveContext.read.parquet(input1)
#only work for bus#135 for now, since schedule and pattern is only hard coded for 135.
    bus='135'
    direction=direction.lower()
    if direction=='east':
        direction='EAST'
    elif direction=='west':
        direction='WEST'
    else:
        print 'Bus #'+bus+' does not this direction.'
        sys.exit()
    query=Master.where((F.to_date(Master.TimeStamp)>=date1)&(F.to_date(Master.TimeStamp)<=date2))
    query=query.where((query.RouteNo==bus)&(query.Direction==direction))

#need to use the Window function to group the same trip together. Because TripId is not unique across multiple dates (it is only unique in one day)
#Hence we need to use the HIVE context
    hourToSecond=(lambda h:h*3600)
    window=Window.partitionBy([query.TripId,query.VehicleNo]).orderBy(F.unix_timestamp(query.TimeStamp)).rangeBetween(-hourToSecond(2),hourToSecond(2))
    FirstTimeStamp=F.first(query.TimeStamp).over(window).alias('T1')
    FirstLongitude=F.first(query.Longitude).over(window).alias('Lon1')
    FirstLatitude=F.first(query.Latitude).over(window).alias('Lat1')
    LastTimeStamp=F.last(query.TimeStamp).over(window).alias('T2')
    LastLongitude=F.last(query.Longitude).over(window).alias('Lon2')
    LastLatitude=F.last(query.Latitude).over(window).alias('Lat2')
    query=query.select(query['*'],FirstTimeStamp,FirstLongitude,FirstLatitude,LastTimeStamp,
	               LastLongitude,LastLatitude)
    query=query.drop(query.TimeStamp).drop(query.Longitude).drop(query.Latitude).dropDuplicates()
    query=query.drop(query.Destination).cache()

#At the end of the grouping. multiple GPS coordinate for any trip is compressed into a single row, which contain the general infromation for the trip
#as well as the start coordinate, start time, finish coordinate and finish time for that trip.

#create a df for the translink Patterns
    schema1=StructType([
        StructField('RouteNo',StringType(),False),
        StructField('Direction',StringType(),False),
        StructField('Pattern',StringType(),False),
        StructField('StartNo',IntegerType(),False),
        StructField('EndNo',IntegerType(),False),
    ])
    pattern_rdd=sc.textFile(input2)
    pattern_rdd=pattern_rdd.map(lambda line:tuple(str.split(str(line),',')))
    pattern_rdd=pattern_rdd.map(lambda (o1,o2,o3,o4,o5):(o1,o2,o3,int(o4),int(o5)))
    pattern_df=hiveContext.createDataFrame(pattern_rdd,schema=schema1)

#join df query with Patterns df to get the start stop number and final stop number for each trip
    condition=[query.RouteNo==pattern_df.RouteNo,query.Direction==pattern_df.Direction,
	       query.Pattern==pattern_df.Pattern]
    query=query.join(pattern_df,condition).drop(pattern_df.RouteNo).drop(pattern_df.Direction).drop(pattern_df.Pattern)
    query=query.drop(query.Pattern)

#generate the end trip time block and day of week for each row(Trip); prepare to join with the ScheduleTable
    DoWudf=F.udf(lambda DT:get_DoW(DT),StringType())
    query=query.withColumn('TimeBlock',F.hour(query.T2)).withColumn('DayofWeek',DoWudf(query.T2))

#join query df with ScheduleTable such that each row(Trip) gets its associate expected triptime according to Translink Schedule
    Scheduled=hiveContext.read.parquet(input3)
    condition2=[query.RouteNo==Scheduled.RouteNo,query.Direction==Scheduled.Direction,
		query.StartNo==Scheduled.StartNo,query.EndNo==Scheduled.EndNo,
		query.TimeBlock==Scheduled.TimeBlock,query.DayofWeek==Scheduled.DayofWeek]
    query=query.join(Scheduled,condition2).drop(Scheduled.RouteNo).drop(Scheduled.Direction).drop(Scheduled.StartNo).drop(Scheduled.EndNo).drop(Scheduled.TimeBlock).drop(Scheduled.DayofWeek).cache()

#write to a temporary parquet for visualization (for plotQ1.py)
    query.coalesce(1).write.parquet(output,mode='overwrite')

if __name__=="__main__":
    input1=sys.argv[1]
    input2=sys.argv[2]
    input3=sys.argv[3]
    output=sys.argv[4]
    date1=sys.argv[5]
    date2=sys.argv[6]
    direction=sys.argv[7]
    try:
	    date_format='%Y-%m-%d'
	    startDate=datetime.datetime.strptime(date1,date_format).date()
	    endDate=datetime.datetime.strptime(date2,date_format).date()
    except ValueError:
	    print 'Invalid date format.'
	    sys.exit()
    conf=SparkConf().setAppName('Q1')
    sc=SparkContext(conf=conf)
    main(sc,input1,input2,input3,output,date1,date2,direction)

#${SPARK_HOME}/bin/spark-submit --master 'local[*]' tripTimeAndCount.py ../MasterTable ../Patterns ../ScheduleTable ../TempTable 2015-10-18 2015-10-31 east
#   input1='../MasterTable' path to the raw Master table
#   input2='../Patterns' path to the 'Patterns' that contain all the patterns text (ie. 135.txt in folder'Patterns')
#   input3='../ScheduleTable' path to the ScheduleTable parquet that containing a df of all scheduled (generated by the updated scheduleTripTime.py)
#   output='../TempTable' path to storing the temporary parquet for visualization (for now)
#   argv[5] is the start date T1 of the query (ie. 2015-10-18)
#   argv[6] is the end date T2 of the query (ie. 2015-10-31)
#   argv[7] is the direction of the bus route os the query (ie. east or west)