#scheduleTripTime.py will update the Schedule parquet regarding to the schedule set by
#Translink. Currently, we only have schedules for bus route 135.
__author__='Bernard Chan'
import sys,datetime
from pyspark import SparkConf, SparkContext
from pyspark.sql.types import *
from pyspark.sql import SQLContext, Row, DataFrame
from pyspark.sql import functions as F

conf = SparkConf().setAppName('Trip Time')
sc = SparkContext(conf=conf)
sqlContext = SQLContext(sc)

inputs = sys.argv[1] #input file
output = sys.argv[2] #output file

text = sc.textFile(inputs)

routeName = inputs.split("/")
routeInfo = routeName[2].split(".") #[0] should be the first part and [1] should be just 'txt'


def lineProcessing(inputLine):
    splitStr = inputLine.strip().split(" ")
    end = splitStr[len(splitStr)-1].split(".")
    start = splitStr[0].split(".")
    if end[0] == start[0]:
        expectedTripTime = int(end[1]) - int(start[1]) #mesaured in minutes
    else:
        expectedTripTime = int(end[1])+60-int(start[1])
    if routeInfo[0][-1:] == 'W' and len(splitStr) == 3:
        startStopNumber = '50530'# Burrard Station
        endStopNumber = '58143'# Kootenay Loop
    elif routeInfo[0][-1:] == 'W' and len(splitStr) == 4:
        startStopNumber = '51743' #Boundary Rd at Kitchener St
        endStopNumber = '52907' #Hastings at Duthie
    elif routeInfo[0][-1:] == 'W' and len(splitStr) == 5:
        startStopNumber = '50530' # Burrard Station
        endStopNumber = '53096' # SFU Exchange Bay 2
    if routeInfo[0][-1:] == 'E' and len(splitStr) == 3:
        startStopNumber = '58143' # Kootney Bay 8
        endStopNumber = '50530' # Burrard Station
    elif routeInfo[0][-1:] == 'E' and len(splitStr) == 5:
        startStopNumber = '53096' # SFU Exchange Bay 2
        endStopNumber = '50530' # Burrard Station
    return [startStopNumber, endStopNumber, len(splitStr), expectedTripTime, routeInfo[0][0:3], routeInfo[0][3:-1], routeInfo[0][-1:], str(end[0])]


rawLines = text.map(lineProcessing) #an rdd

proceesedLines = rawLines.collect()


noonYet = 0
previous = '0'
for i in proceesedLines:
    if previous != '12' and i[len(i)-1] == '12':
        noonYet += 1
    previous = i[len(i)-1]
    if noonYet == 0:
        i.append(i[len(i)-1]+'am')
    elif noonYet == 1:
        i.append(i[len(i)-1]+'pm')
    else:
        i.append(i[len(i)-1]+'am')

    # print i


def add_pairs((a, b), (c, d)):
    return (a+c, b+d)

scheduleRDD = sc.parallelize(proceesedLines)
scheduleCount = scheduleRDD.map(lambda w: ((w[len(w)-1], w[2]), (1, w[3])))
scheduleReduced = scheduleCount.reduceByKey(add_pairs)
scheduleAveraged = scheduleReduced.mapValues(lambda (count,totalT): (count, 1.0*totalT/count))
scheduleUnpacked = scheduleAveraged.map(lambda ((a, b),(c,d)): (a, b, c,d))

schema1 = StructType([
    StructField("TimeBlock", StringType(), True),
    StructField("Stops", StringType(), True),
    StructField("BusCount", IntegerType(), True),
    StructField("AverageExpectedTripTime", FloatType(), True),
    ])

averageTimeDF = sqlContext.createDataFrame(scheduleUnpacked, schema1)

#averageTimeDF.show(50)



schema2 = StructType([
    StructField("Start", StringType(), True),
    StructField("End", StringType(), True),
    StructField("Stops", IntegerType(), True),
    StructField("Expected", IntegerType(), True),
    StructField("RouteNumber", StringType(), True),
    StructField("DayofWeek", StringType(), True),
    StructField("Direction", StringType(), True),
    StructField("EndHour", StringType(), True),
    StructField("TimeBlock", StringType(), True),
    ])

scheduleDF = sqlContext.createDataFrame(scheduleRDD, schema2)

#scheduleDF.show(50)

joinedDF = scheduleDF.join(averageTimeDF, ['Stops', 'TimeBlock']).drop(scheduleDF.Expected).drop(scheduleDF['EndHour']).drop(scheduleDF['Stops']).cache()

joinedDF=joinedDF.dropDuplicates()

#Transklink define East and West differently in their DB and their publicly available bus schedule.
def changeD(D):
	if D=='W':
		return 'EAST'
	elif D=='E':
		return 'WEST'

def changeT(T):
	time_format='%I%p'
	return datetime.datetime.strptime(T,time_format).hour

changeDudf=F.udf(lambda direction:changeD(direction),StringType())
changeTudf=F.udf(lambda time:changeT(time),IntegerType())

joinedDF=joinedDF.select(joinedDF.Start.cast('int').alias('StartNo'),joinedDF.End.cast('int').alias('EndNo'),joinedDF.RouteNumber.alias('RouteNo'),joinedDF.DayofWeek,changeDudf(joinedDF.Direction).alias('Direction'),changeTudf(joinedDF.TimeBlock).alias('TimeBlock'),joinedDF.BusCount,joinedDF.AverageExpectedTripTime.alias('AvgT'))

joinedDF.orderBy(joinedDF.TimeBlock).coalesce(1).write.parquet(output,mode='append')

#joinedDF.show(250)





#${SPARK_HOME}/bin/spark-submit --master local scheduleTripTime.py ../schedules/135WeekdayW.txt ../ScheduleTable
#inputs='../schedules/135WeekdayW.txt'    Yes, use 135WeekdayW for now, because 135WeekdayW schedule is actually 135WeekdayE
#output='../Scheduled'  path to where to store the SCheduled parquet
#possible schedule files are:135WeekdayW.txt 135WeekdayE.txt 135SatW.txt 135SatE.txt 135SunW.txt 135SunE.txt