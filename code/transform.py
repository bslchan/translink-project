#This code transform the json input into the correct format, and create a parquet file which stores the MasterTable
#Also assign the right timestamp to every GPS row, since the json object returned by Translink's API doesn't have a date in it.
__author__='Jacky Liao'
from pyspark import SparkConf,SparkContext,sql
from pyspark.sql.types import StructType,StructField,StringType,DateType,IntegerType,FloatType,TimestampType
import sys,re,datetime,json, glob

#apply the schema to raw json file
def raw_applySchema(j):
    APItimestamp=j['APItimestamp']
    APItime_format='%Y-%m-%d_%H-%M-%S'
    APItimestamp=datetime.datetime.strptime(APItimestamp,APItime_format)
    date_format='%Y-%m-%d'
    APIdate=datetime.datetime.strftime(APItimestamp.date(),date_format)
    RecordedTime=APIdate+' '+j['RecordedTime']
    timestamp12h_format='%Y-%m-%d %I:%M:%S %p'
    RecordedTime=datetime.datetime.strptime(RecordedTime,timestamp12h_format)
    #fix the api call around midnight, since translink api doesn't have a date attribute
    if APItimestamp<RecordedTime:
        RecordedTime=RecordedTime+datetime.timedelta(days=-1)
    return (RecordedTime,j['RouteNo'],j['Direction'],int(j['TripId']),j['Pattern'],j['Destination'],
            float(j['Longitude']),float(j['Latitude']),j['VehicleNo'])

def main(sc,input,output,startDate,endDate):
    schema1=StructType([
        StructField('TimeStamp',TimestampType(),False),
        StructField('RouteNo',StringType(),False),
        StructField('Direction',StringType(),False),
        StructField('TripId',IntegerType(),False),
        StructField('Pattern',StringType(),False),
        StructField('Destination',StringType(),False),
        StructField('Longitude',FloatType(),False),
        StructField('Latitude',FloatType(),False),
        StructField('VehicleNo',StringType(),False),
    ])
    #load raw data to data frame
    sqlContext=sql.SQLContext(sc)
    dates_list=[startDate+datetime.timedelta(days=x) for x in range(0,(endDate-startDate).days+1)]
    records_df=None
    #join all the json files into one big df.
    for date in dates_list:
        date_str=datetime.datetime.strftime(date,date_format)
        files=glob.glob(input+'/'+date_str+'.json')
        if files!=[]:
            jline=sc.textFile(input+'/'+date_str+'.json')
            jline=jline.map(lambda j:json.loads(j)).flatMap(lambda j:j)
            jline=jline.map(lambda j:raw_applySchema(j))
            df=sqlContext.createDataFrame(jline,schema1)
            if records_df!=None:
                records_df=records_df.unionAll(df).cache()
            else:
                records_df=df.cache()
    #Droping duplicates gives less entries because bus GPS doen't always update the translink API server every 2 minutes.
    records_df=records_df.dropDuplicates().orderBy(records_df.TimeStamp)
    #Append to parquet file if the MasterTable already existed
    records_df.coalesce(1).write.parquet(output,mode='append')

if __name__=="__main__":
    input=sys.argv[1]
    output=sys.argv[2]
    date1=sys.argv[3]
    date2=sys.argv[4]
    #check argv[3] and argv[4] for correct input format
    try:
	    date_format='%Y-%m-%d'
	    startDate=datetime.datetime.strptime(date1,date_format).date()
	    endDate=datetime.datetime.strptime(date2,date_format).date()
    except ValueError:
	    print 'Invalid date format.'
	    sys.exit()
    conf=SparkConf().setAppName('data transform')
    sc=SparkContext(conf=conf)
    main(sc,input,output,startDate,endDate)

#${SPARK_HOME}/bin/spark-submit --master 'local[*]' transform.py ../Raw_files ../MasterTable 2015-10-18 2015-10-31
#input='../Raw_files' Path to the concated files
#output='../MasterTable' Path to where to put the parquet containing the Raw Master Table
#argv[3] and argv[4] are start date and end date. (ie. 2015-10-18)