#This code cocat all the small chunks of json files into one json file group by date
__author__='Jacky Liao'
import sys, json, datetime, glob, os
path=sys.argv[1]
output=sys.argv[2]
date1=sys.argv[3]
date2=sys.argv[4]

#check argv[3] and argv[4] for correct input format
try:
	date_format='%Y-%m-%d'
	startDate=datetime.datetime.strptime(date1,date_format).date()
	endDate=datetime.datetime.strptime(date2,date_format).date()
	dates_list=[startDate+datetime.timedelta(days=x) for x in range(0,(endDate-startDate).days+1)]
except ValueError:
	print 'Invalid date format.'
	sys.exit()

#create output directory if needed
if os.path.exists(output)==False:
    os.makedirs(output)

#concat.
for date in dates_list:
        date_str=datetime.datetime.strftime(date,date_format)
        files=glob.glob(path+'/'+date_str+'_*')
        if files==[]:
        	print "Can't find Date:"+date_str
        else:
        	my_file=open(output+'/'+date_str+'.json','w')
        	result=[]
        	l=0
        	for f in files:
                	jfile=open(f,'r')
                	jlist=json.load(jfile)
                	jfile.close()
                	APItimestamp=str.split(f,'/').pop()
                	APItimestamp=str.split(APItimestamp,'.')[0]
                	# add in api-timestamp to each bus gps json object
                	for j in jlist:
                		j['APItimestamp']=APItimestamp
                		result.append(j)
                	l+=len(jlist)
        	json.dump(result,my_file)
        	if len(result)!=l:
                	print "Can't find Date:"+date_str
        	my_file.close()

#python concat_bydate.py ../RAW ../Raw_files 2015-10-18 2015-10-31
#input='../RAW'  path to all the chucks of small JSON files
#output='../Raw_files'
#argv[3] and argv[4] are start date and end date. (ie. 2015-10-18)