#bunching.py will find all the trips from a date set by user, and plot the result
#to a distance vs. time plot such that user can use it to identify bunching.
__author__='Bernard Chan'
from pyspark import SparkConf,SparkContext,sql
from pyspark.sql.types import StructType, StructField, StringType, DateType, IntegerType, FloatType
from pyspark.sql import functions as F
import sys,re,datetime
from getDistance import distance_on_unit_sphere
import matplotlib.pyplot as plt
import matplotlib


def get_time(TS):
    time_format='%I:%M:%S %p'
    time=datetime.datetime.strftime(TS,time_format)
    return time

def main(sc,input,date):
    #load raw data to data frame
    sqlContext=sql.SQLContext(sc)
    df=sqlContext.read.parquet(input)
    get_timeUDF=F.udf(lambda TS:get_time(TS),StringType())
    df=df.withColumn('RecordedTime',get_timeUDF(df.TimeStamp))
    df=df.where(F.to_date(df.TimeStamp)==date)

    #if cache this df, df.filter("RouteNo = '135'").filter("Direction = 'EAST'") will return empty
    #bug maybe?

    tripsW = df.filter("RouteNo = '135'").filter("Direction = 'WEST'")\
                .drop(df.TimeStamp).drop(df.Direction).drop(df.Pattern).drop(df.VehicleNo)\
                .drop(df.Destination).drop(df.Latitude).drop(df.Longitude).drop(df.RouteNo)\
                .drop(df.RecordedTime).dropDuplicates(['TripId'])

    tripsE = df.filter("RouteNo = '135'").filter("Direction = 'EAST'")\
                .drop(df.TimeStamp).drop(df.Direction).drop(df.Pattern).drop(df.VehicleNo)\
                .drop(df.Destination).drop(df.Latitude).drop(df.Longitude).drop(df.RouteNo)\
                .drop(df.RecordedTime).dropDuplicates(['TripId'])

    tripsIdE = tripsE.map(lambda p: p.TripId).collect() # each id is an int

    filteredE = df.filter("RouteNo = '135'").filter("Direction = 'EAST'")\
            .drop(df.TimeStamp).drop(df.Direction)\
            .drop(df.Destination).drop(df.Pattern).drop(df.VehicleNo).drop(df.RouteNo)

    filteredE.cache()

    filteredW = df.filter("RouteNo = '135'").filter("Direction = 'WEST'")\
            .drop(df.TimeStamp).drop(df.Direction)\
            .drop(df.Destination).drop(df.Pattern).drop(df.VehicleNo).drop(df.RouteNo)

    def dataPoints (masterTable, tripId):
        #filter out based on tripID and extract lat, long and time info
        oneBusTrip = masterTable.filter('TripId = %u' %tripId) #this is a df
        latList = oneBusTrip.map(lambda p: p.Latitude).collect() # this is  a list
        longList = oneBusTrip.map(lambda p: p.Longitude).collect()
        timeList = oneBusTrip.map(lambda p: p.RecordedTime).collect()
        distanceList = [0]
        distance = 0
        #calculate the total distance traveled
        for i in xrange (0,len(latList)-1):
            distance += distance_on_unit_sphere(latList[i], longList[i], latList[i+1], longList[i+1])
            distanceList.append(distance)
        dateTimeList = []
        dateTimeRawList = []
        format='%Y-%m-%d %I:%M:%S %p'
        #find the time stamp
        for timeStamp in timeList:
            formattedTime = datetime.datetime.strptime(date+' '+timeStamp, format)
            convertedTime= matplotlib.dates.date2num(formattedTime)
            dateTimeList.append(convertedTime)
            dateTimeRawList.append(formattedTime)
        return dateTimeList, distanceList, dateTimeRawList



    dateTimeList =[]
    distanceList = []
    rawTimeList = []

    #for each trip going EAST get the data points of distance travel and time on the road
    for id in tripsIdE:
        dateTimeTemp, distanceTemp, rawTimeListTemp = dataPoints(filteredE, id)
        dateTimeList.append(dateTimeTemp)
        distanceList.append(distanceTemp)
        rawTimeList.append(rawTimeListTemp)


    #plot the distance versus time
    for i in xrange(0,len(dateTimeList)-1):
        elapsedTime = rawTimeList[i][len(rawTimeList[i])-1]-rawTimeList[i][0]
        totalMinutes = divmod(elapsedTime.total_seconds(), 60)[0]
        totalDistance = distanceList[i][len(distanceList[i])-1]
        if totalDistance < 20 and totalMinutes < 90:
            plt.plot_date(dateTimeList[i], distanceList[i],'o-')

    #plt.xlabel('Time')
    plt.ylabel('Distance (km)')
    plt.title('Bus Trajectories on '+date +' for 135 East')

    font = {'family' : 'normal',
        'weight' : 'bold',
        'size'   : 35}

    matplotlib.rc('font', **font)
    plt.grid(True)

    # specify a rotation for the tick labels
    ax = plt.axes()
    plt.xticks(rotation=30)
    plt.subplots_adjust(bottom=0.2)
    max_xticks = 5
    xloc = plt.MaxNLocator(max_xticks)
    ax.xaxis.set_minor_locator(xloc)
    plt.show()




if __name__=="__main__":
    input=sys.argv[1]
    date=sys.argv[2]
    conf=SparkConf().setAppName('data transform')
    sc=SparkContext(conf=conf)
    main(sc,input,date)


#${SPARK_HOME}/bin/spark-submit --master 'local[*]' bunching.py ../MasterTable 2015-10-20
#   input='../MasterTable' path to the raw Master table
#   date='2015-10-20' the date for which the user wants to observe bunching.