\documentclass[11pt,]{article}
\usepackage[top=1.in,bottom=1.in,left=.75in,right=.75in]{geometry}
\usepackage{graphicx}
\usepackage[numbers]{natbib}
\usepackage{verbatim}
\usepackage{amssymb,amsmath}
\usepackage{slashbox}
\usepackage{mathtools}
\newcommand\ceil[1]{\lceil#1\rceil}
\usepackage[affil-it]{authblk} 
\usepackage{dirtytalk}
\usepackage{rotating}
\usepackage{tikz}
\usepackage{thm} 
\usepackage{sectsty}
\usepackage{wrapfig}
\usepackage[labelformat=simple]{subcaption}
\renewcommand\thesubfigure{(\alph{subfigure})}
\newtheorem{definition}{Definition}
\title{\vspace{-2cm}\large CMPT 732 Project Report: A Data-Driven Analysis on Public Transit for SFU Burnaby}
\author{\vskip-.1in Bernard S. Chan (bernardc@sfu.ca) and Jacky Liao (zjliao@sfu.ca)}
    \date{\vskip-.2in\today\vskip-.75in}

\sectionfont{\fontsize{11}{15}\selectfont}
\subsectionfont{\fontsize{11}{12}\selectfont}

\let\OLDthebibliography\thebibliography
\renewcommand\thebibliography[1]{
  \OLDthebibliography{#1}
  \setlength{\parskip}{0pt}
  \setlength{\itemsep}{0pt plus 0.3ex}
}

\usepackage{titlesec}
\titleformat{\section}
 {\bf\fontsize{11}{11}}{\thesection}{1em}{}
\titleformat{\subsection}[runin]
 {\bf\fontsize{11}{11}}{\thesubsection}{1em}{}[:]
\titleformat{\subsubsection}[runin]
 {\normalfont\fontsize{12}{15}}{\thesection}{1em}{}[]
\setlength{\bibsep}{0pt}
\makeatother
\renewcommand{\abstractname}{Project Executive Summary}
\begin{document}
\maketitle
\begin{comment}
\begin{abstract}
asdf
\end{abstract}
\end{comment}
\vspace{-1cm}
\section{\normalsize Introduction}
\label{sec:introduction}
\vskip-.1in
Due to cost and lack of parking infrastructure, many Simon Fraser University (SFU) Burnaby students choose to commute via buses. Specifically, there are four bus routes (135, 143, 144, and 145) from TransLink, the transportation authority for Metro Vancouver, that service SFU Burnaby. During rush hours, students often complain that buses would overflow. Irregular bus schedule is another common complaint. TransLink has tried to address these issues by increasing service. The increased service have no observable impact on the problem, but it has increased emission and traffic congestions. Due to both ridership and environmental concerns, we are motivated to study the public transportations connecting SFU Burnaby and greater Vancouver through data-driven methods.

Based on conversations with representatives from Embark, a sustainability focused student led organization at SFU, we learned that there are many perceived problems surrounding buses servicing Burnaby Mountain. For students who live near the university, they often have trouble getting on the bus during rush hours. This phenomenon is known as a pass-up. SFU is aware of this problem and it has a webpage\footnote{SFU's webpage on bus pass-up is https://www.sfu.ca/busstop.html.} dedicated to collecting information on it. Besides the bus pass-up problems, many students feel that the service from TransLink is poor due to longer than scheduled trip times and irregular service. 

Aside from speaking with student organizations, we also spoke with~\citet{Ohnemus} of TransLink about their services to Burnaby Mountain. TransLink is aware of the students' complaints and they have tried to improve their services. As the regional authority on public transit, TransLink is more concerned with higher level problems in their service. Specifically, they are concerned with buses failing to follow the schedule due to bunching. Ideally, buses would depart from origin at regular intervals and they would arrive at the destination at the same rate. Due to traffic conditions, later buses may catch up to an earlier bus and this phenomenon is called bunching. Bunching is undesirable because most passengers are concentrated on the earlier bus. Furthermore, it would increase the wait time for users later in the route. 

Given the numerous transit problems between Burnaby Mountain and greater Vancouver, we will use data-driven approach to study these woes. In this report, we will outline the goals and challenges of this project in Section~\ref{sec:goals}. Our methods in collecting and analyzing data will be discussed in Section~\ref{sec:method}. Results from the analyses are shown in Section~\ref{sec:results}. Recommendations from analytics are discussed in Section~\ref{sec:discussion}.

\vspace{-.25cm}
\section {Project Goals and Challenges}
\label{sec:goals}
\vskip-.1in
For this project, we would like to understand the underlying mechanisms of the transit woes surrounding Burnaby Mountain. Specifically, we would like to find trends and patterns on these problems so that they can be used in the future planning. Our main goal is to discover hourly, daily and weekly patterns on total trip time, number of buses operated, and any interesting trends. 

There are several challenges associated with this project. As with any data-driven investigation, the first requirement is to procure a reliable data set. Since TransLink's  databases on buses and passengers are not public available, we can only extract information form its public API. The API provides a limited amount of information so it limits the questions we can answer. For example, the API does not contain any information on ridership, so we cannot answer any question on capacity. To further complicate matter, there is a daily limit on requests to the database. This limit lowers the resolution of our data to one request every two minutes and it lowers the quality of our data. Moreover, information obtained through the API can contain error. Finally, documentation on the API is incomplete, so we have to interpret the data as we proceed. 


\section{\normalsize Methodology}
\label{sec:method}
\vskip-.1in
In this section, we provide detailed descriptions of the methods used in this report. Our methods in data collection are discussed in Section~\ref{sec:dataCollection}. Since the data was originally in JSON format and we needed them to be usable for Spark and Python, our data extraction and transformation procedures in described Section~\ref{sec:ETL}. Methods used for data analysis are described in Section~\ref{sec:analysis}. 

\vspace{-.25cm}
\subsection{Data Collection}
\label{sec:dataCollection}
\vskip-.1in
While TransLink publishes an annual performance report on all bus routes~\citep{translink2014}, the data as well as the methodology behind this publication are not available to the public. We requested for TransLink's internal database, but it was not successful. Hence, we can only rely on the data from the public API. We used \emph{Cron}, a time-based job scheduler for Unix-like systems, to run a Python script to request information from the TransLink's API and saved the information to disk. Real-�time information on  buses were collected over several weeks. 

Given that we do not have any passenger information from TransLink, we tried to obtain passenger information from SFU. As mentioned in Section~\ref{sec:introduction}, SFU has been collecting information on bus problems affecting its students. Through communication with~\citet{privateCommunication} of SFU's ancillary services, we obtained the data collected. This database was collected over a year and there were 263 entries. It contained time stamps and comments from the students whenever bus pass-ups occurred. Due to the lack of data points and geographical information, we did not incorporate this database into our analysis. 

\vspace{-.25cm}
\subsection{Extract, Transform, and Load}
\label{sec:ETL}
\vskip-.1in
Each request to the API produced one file in JSON format. Since we made many requests per day, there are many small files. 
For the analysis, we are interested in time span on the order of days to weeks, so the first task was to combined this information into a more useful form. The script \emph{concat\_bydate.py} concatenates all the small files for each day into one congruent file and outputs it into a specified directory. Since the data provided is in JSON format, so we wrote \emph{transform.py} to transform the information into Parquet files. 

One of our question was whether the bus traffic pattern from collected data match the published schedules. The schedules for the buses were downloaded from TransLink's website as \emph{pdf} files. Since the \emph{pdf} files were formatted inconveniently for analysis, they were manually transcribed into text files. These text files were fed through \emph{scheduleTripTime.py} and they were also converted into Parquet files for analysis. 

We chose Spark to extract the information from the collected data because a JSON format reader is readily available. Secondly, we chose the Parquet format because it is a well integrated part of Spark. Furthermore, this format is also convenient for storage and easily reused for different parts of the analysis. 

\vspace{-.25cm}
\subsection{Analysis}
\label{sec:analysis}
\vskip-.1in
For the analysis on bunching, we wanted to understand the phenomenon visually. Inspired from the work by~\citet{lizana2014bus}, we plotted the bus trajectories of a day using \emph{bunching.py}. In this script, we inputted the aforementioned Parquet files from ETL. For a given trip of the bus, we have latitude and longitude information roughly every two minutes. Let $coord_1=(latitude_1, longitude_1)$ and $coord_2=(latitude_2, longitude_2)$ be the coordinates of any two consecutive data points and 
\begin{equation*}
a=\sin^2{(latitude_2-latitude_1)}+\cos{(latitude_1)}\cos{(latitude_2)}\sin^2{(longitude_2-longitude_1)}
\end{equation*} be the great-circle distance between $coord_1$ and $coord_2$ as calculated by the Haversine formula.  Via \emph{getDistance.py}, the distance traveled between each data point was estimated using the following formula:
\begin{equation*}
d(coord_1,coord_2)=c*R,
\end{equation*}
where $c=2*\arctan{\left(\sqrt{a}, \sqrt{1-a}\right)}$ and $R=6373\text{km}$ is the radius of the earth. Based on \emph{tripID}, a unique identifier for each trip on a given day, the total distance per trip was estimated by adding all distances between data points. Since our work thus far has been done through the Spark Python API and \emph{matplotlib}  is a mature Python library for visualizations, we used this tool for the plots. 

In order to understand whether the buses followed the published schedule, we analyzed the official schedules published on TransLink's website. First, we counted the number of trips that were suppose to occur per hour. Then, we calculated the average total trip time in each hour. Since there are several different route patterns for each route and the trip time varied throughout the day, the average trip times for different patterns at each hour of the day were calculated. For this work, we used a combination of Spark and Python in \emph{scheduleTripTime.py} to perform the analysis. Spark was used to reduce the data to manageable size. Since operations between rows of tables were needed, we switched to Python for parts of the analysis. Python was an appropriate choice because  we have already reduced the data size through Spark and the official schedules are fixed in sizes as well. Furthermore, operations between rows were necessary and Spark, even with the dataframe module, was not ideal for this type of work. 

After obtaining the official number of trips and average trip times, we calculated the same information on the collected data. In order to compare our results, the collected data was put into a Spark dataframe and it was then merged with the dataframe obtained through the published schedules. Thus, the published results provided a baseline for each hour of the day on the number of trips and the length each trip should take. The baseline results were compared with results averaged over one week. For this portion of the project, a combination of Spark, Hive and \emph{matplotlib} were used. Within Spark, we used Hive context because the identification number for each trip is only unique over one day. In other words, identification for each trip is reused day after day. In order to distinguish the trips from different days with the same identification number, we needed to partition the dataframe through the \emph{window} function through the Hive context. 
\vspace{-.5cm}
\section{\normalsize Results}
\label{sec:results}
\vskip-.1in

While we collected data on all bus routes for TransLink, due to time and resource constraints, we only analyzed the traffic for route 135 in this report. However, our methods can be extended to study all bus routes of TransLink. Based on our methods described in Section~\ref{sec:method}, we were able to produce several plots that help us understand the issues related to the transit problems mentioned in Section~\ref{sec:introduction}. In Section~\ref{sec:bunching}, we will show the graphical results related to bunching. Then, in Sections~\ref{sec:schedule}, we will graphically compare the publicized number of trips and average trip time versus the actual results obtained from collected data.


\vspace{-.25cm}
\subsection{Bunching}
\label{sec:bunching}
\vskip-.1in
Given the difference in traffic volume between a weekday and over the weekend, we randomly selected a week and plotted bus trajectories for a weekday and a day from the weekend in Figure~\ref{fig:busTrajectories}. Due to the resolution of our data set being limited to one data point per two minutes, we plotted the trajectories of Route 135 because it travels mostly in a straight line on Hastings St. from Burrard Station to SFU.  Figures~\ref{fig:20151020full} and~\ref{fig:20151025full} show the bus trajectories over the entire day of a weekday and the weekend, respectively. Given that bunching tends to occur more often during rush hours, Figures~\ref{fig:20151020part} and~\ref{fig:20151025part} show bus trajectories from 0700 to 1000 of a weekday and the weekend, respectively. 

\begin{figure}[h]
\centering
% left bottom right top
 %%%%%%%%%%%%%%%
  \begin{subfigure}{.46\textwidth}
  \centering
\includegraphics[width=\textwidth, trim={8cm 1cm 5cm 6cm}]{20151020full.png}
\caption{}
\label{fig:20151020full}
\end{subfigure}\qquad\quad 
 %%%%%%%%%%%%%%%
  \begin{subfigure}{.46\textwidth}
  \centering
\includegraphics[width=\textwidth, trim={8cm 1cm 5cm 6cm}]{20151020part.png}
\caption{}
\label{fig:20151020part}
\end{subfigure}%
\vskip .45in
 %%%%%%%%%%%%%%%
 \begin{subfigure}{.46\textwidth}
  \centering
\includegraphics[width=\textwidth, trim={8cm 1cm 5cm 6cm}]{20151024full.png}
\caption{}
\label{fig:20151025full}
\end{subfigure}\qquad\quad 
 %%%%%%%%%%%%%%%
  \begin{subfigure}{.46\textwidth}
  \centering
\includegraphics[width=\textwidth, trim={8cm 1cm 5cm 6cm}]{20151024part.png}
\caption{}
\label{fig:20151025part}
\end{subfigure}%
 \caption{Bus trajectories plots for 135 East. (a): 2015-10-20 (Tuesday). (b): Morning rush hours on 2015-10-20 (Tuesday). (c): 2015-10-24 (Saturday). (d): Morning rush hours on 2015-10-25 (Saturday).}
\label{fig:busTrajectories}
\end{figure}
\subsection{Number of Trips and Average Trip Time}
To understand if the buses run according to schedule, we plotted the number of trips and the average time for each trip in each hour in Figure~\ref{fig:countAndtripTime}. We only plotted the data from weekdays in the aforementioned figure because as seen in Figure~\ref{fig:busTrajectories}, the data from weekends are less interesting. The actual and the scheduled number of buses operated in each hour of the day are plotted in the leftmost and the middle panels of Figure~\ref{fig:avgCount}, respectively. The difference between the actual number subtracting the scheduled number of buses are plotted the rightmost panel of Figure~\ref{fig:avgCount}. Similarly, the actual and the scheduled average trip times per hour were plotted in the leftmost and the middle panels of Figure~\ref{fig:avgTripTime}, respectively. The difference between the actual and scheduled number of buses are plotted the rightmost panel of Figure~\ref{fig:avgCount}.
\label{sec:schedule}
\begin{figure}[h]
\centering
  \begin{subfigure}{.46\textwidth}
\centering
\includegraphics[width=\textwidth, trim={8cm 1cm 5cm 6cm}]{avgCount.png}
\caption{}
\label{fig:avgCount}
\end{subfigure}%
\qquad\quad
\begin{subfigure}{.46\textwidth}
\includegraphics[width=\textwidth, trim={8cm 1cm 5cm 6cm}]{avgTripTime.png}
\caption{}
\label{fig:avgTripTime}
\end{subfigure}
\caption{(a): Number of trips for the 135 E during particular hour of day from 2015-10-19 to 2015-10-30. (b): Average total trip time for the 135 E during particular hour of day from 2015-10-19 to 2015-10-30.}
\label{fig:countAndtripTime}
\end{figure}

\section{\normalsize Discussion}
\label{sec:discussion}
\vskip-.1in
Given that route 135 does not operate 0200 and restart around 0600, the gap shown in Figures~\ref{fig:20151020full} and~\ref{fig:20151025full} provide reenforcement that the data collected are reliable. Furthermore, there are more buses on operating on weekdays than on weekends. This fact is confirmed by the fact that there are more trajectory lines in Figures~\ref{fig:20151020full} than in Figure~\ref{fig:20151025full}.

Through the plots in Figure~\ref{fig:busTrajectories}, we may interpret when two or more trajectory lines intersect as an occurrence of bunching. As seen from Figures~\ref{fig:20151020full} and~\ref{fig:20151020part}, bunching occurs regularly on weekdays. While not shown in this report, trajectory plots of other weekdays show similar behavior. In comparison the observations over the weekend (Figures~\ref{fig:20151025full} and~\ref{fig:20151025part}), we see that bunching simply do not occur. Based on the fact that there is less traffic and congestions on the roads during the weekend, it is likely that bunching occurs due to traffic conditions. 

The effects of bunching on bus services can be readily observed from Figures~\ref{fig:20151020part} and~\ref{fig:20151025part}. The trajectories shown in Figure~\ref{fig:20151025part} are nearly parallel to each other throughout the entire route. This observation implies that the transit users on the entire route are getting service regularly. On the other hand, trajectories shown in Figure~\ref{fig:20151020part} often intersect near the 8km mark. Therefore users near the end of the route, i.e., from 12 km onwards, are not getting service on a regular interval. Two or more buses may arrive in a span of less than 10 minutes and sometimes buses may not arrive for more than 20 minutes. While TransLink has clearly made an effort to improve service by increasing the number of runs, these added runs add to the existing traffic and this new volume leads to more congestions and potentially, bunching. Therefore, TransLink's effort may actually be counterproductive and lead to the appearance of irregularly scheduled buses. 

By comparing the actual number of buses ran over weekdays in Figure~\ref{fig:avgCount}, we found that TransLink typically ran more buses than listed on their schedule. This phenomenon could be a result of TransLink running more buses to keep up with demand. Alternatively, it could be a result of bunching. Since some buses are delayed, more buses than listed are needed in order to fulfill the actual schedule. 

We also averaged the actual trip time for bus trips on weekdays over the same period and the results. As shown by the rightmost panel of Figure~\ref{fig:avgTripTime}, the buses typically finish sooner than the time listed by TransLink. This result would contradict the expectations of many students as SFU. Since the results are averaged over two weeks worth of weekdays, we believe that our results resemble reality. One may interpret the fact that buses are finishing sooner because the drivers are trying finish each run as quickly as possible alleviate the load problem. However, fact that the drivers are finishing sooner than expected may contribute to poor service. Since all times averaged, the faster finishing times may come from the buses that are catching up to slower buses from before. 

For future work, we would like to produce interactive visualizations that incorporate geographical information. Furthermore, we would like to change our scripts so that we may directly compare the traffic loads between various dates directly. Finally, we would like to incorporate ridership information into this analysis. 


\section{Acknowledgement}
\vskip-.1in
First and foremost, we thank our instructor Greg Baker for his guidance throughout the project. Our TAs, Syed Jishan and Golnar Sheikhshabbafghi, also helped usovercome various technical problems encountered in this project. We would also like to thank Embark for their continual support on this project. Their inputs and suggestions helped enhanced the applicability. Furthermore, we thank Michael Ohnemus of TransLink in suggesting that we investigate the bunching phenomenon and Mark McLaughlin of Ancillary Services at SFU for providing us with their data on students' comments.  

\renewcommand\refname{} %hiding the name
\vspace{-.25cm}
\section*{References:}
\vspace{-1.2cm}
\bibliographystyle{plainnat}
\bibliography{myRef}
\newpage
\vspace{-.25cm}
\section{Project Summary}

\begin{tabular}{|c|c|}\hline
Category&Priority Points\\\hline
Getting the Data & 3\\\hline
ETL&5\\\hline
Problem&5\\\hline
Algorithmic Work&0\\\hline
Bigness&2\\\hline
UI&0\\\hline
Visualization&4\\\hline
Technologies&1\\\hline
\end{tabular}

\end{document}  